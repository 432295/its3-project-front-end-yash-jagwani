import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CustomerListComponent } from './component/customer-list/customer-list.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { BookListComponent } from './component/book-list/book-list.component';
import { AuthorListComponent } from './component/author-list/author-list.component';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './component/home/home.component';
import { AboutComponent } from './component/about/about.component';
import { ContactComponent } from './component/contact/contact.component';
import { CustomerFormComponent } from './component/customer-form/customer-form.component';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { RegisterComponent } from './component/register/register.component';
import { LoginComponent } from './component/login/login.component';
import { AlertComponent } from './component/alert/alert.component';
import { ReactiveFormsModule } from '@angular/forms';
// import { JwtInterceptor } from './helper/jwt.interceptor';
// import { ErrorInterceptor } from './helper/error.interceptor';
import { ChatComponent } from './component/chat/chat.component';
import { ProfileComponent } from './component/profile/profile.component';
import { BoardAdminComponent } from './component/board-admin/board-admin.component';
import { BoardUserComponent } from './component/board-user/board-user.component';

import { authInterceptorProviders } from './helper/auth.interceptor';
import { BookFormComponent } from './component/book-form/book-form.component';


library.add(fas);

@NgModule({
  declarations: [
    AppComponent,
    CustomerListComponent,
    MainNavComponent,
    BookListComponent,
    AuthorListComponent,
    HomeComponent,
    AboutComponent,
    ContactComponent,
    CustomerFormComponent,
    RegisterComponent,
    LoginComponent,
    AlertComponent,
    ChatComponent,
    ProfileComponent,
    BoardAdminComponent,
    BoardUserComponent,
    BookFormComponent,

  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    RouterModule,
  ],
  providers: [
    
              authInterceptorProviders
    // { 
    //   provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true 
    // },
    // { 
    //   provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true 
    // },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
