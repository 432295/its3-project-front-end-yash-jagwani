import { Book } from "./Book"

export class Customer {
    "customerId": number
    "customerNr": String
    "customerName": String
    "books": Set<Book>
};
    //Original
    /*"book":"Mathematics and The Physical World"
    "customerNr":"C1"
    "id":1
    "name":"Yash Jagwani"};*/