import { Customer } from "./Customer"

export class Book {
    "bookId": number
    "bookNr": String
    "bookName": String
    "author": String
    "genre": String
    "price": number
    "availability": boolean
};