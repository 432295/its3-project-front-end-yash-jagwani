import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthorListComponent } from './component/author-list/author-list.component';
import { BookListComponent } from './component/book-list/book-list.component';
import { CustomerListComponent } from './component/customer-list/customer-list.component';
import { CustomerFormComponent } from './component/customer-form/customer-form.component';
import { HomeComponent } from './component/home/home.component';
import { AboutComponent } from './component/about/about.component';
import { ContactComponent } from './component/contact/contact.component';
import { LoginComponent } from './component/login/login.component';
import { RegisterComponent } from './component/register/register.component';
// import { AuthGuard } from './helper/auth.guard';
import { ChatComponent } from './component/chat/chat.component';
import { ProfileComponent } from './component/profile/profile.component';
import { BoardUserComponent } from './component/board-user/board-user.component';
import { BoardAdminComponent } from './component/board-admin/board-admin.component';
import { BookFormComponent } from './component/book-form/book-form.component';

const routes: Routes = [
  // {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'customers', component: CustomerListComponent},
  {path: 'addcustomer', component: CustomerFormComponent},
  {path: 'books', component: BookListComponent},
  {path: 'addbook', component: BookFormComponent},
  {path: 'authors', component: AuthorListComponent},
  {path: 'about', component: AboutComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'chat', component: ChatComponent},
  {path: 'profile', component: ProfileComponent },
  {path: 'user', component: BoardUserComponent },
  {path: 'admin', component: BoardAdminComponent },
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
