import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from 'src/app/model/Book';
import { BooksService } from 'src/app/service/books.service';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.scss']
})
export class BookFormComponent {

  book: Book;

  constructor(
    private route: ActivatedRoute, 
      private router: Router, 
        private bookService: BooksService) {
    this.book = new Book();
   }

   onSubmit() {
     this.bookService.save(this.book).subscribe(result => this.gotoBookList());
   }

   gotoBookList() {
     this.router.navigate(['/books']);
   }

}
