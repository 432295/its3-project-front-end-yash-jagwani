import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Author } from 'src/app/model/Author';
import { AuthorsService } from 'src/app/service/authors.service';

@Component({
  selector: 'app-author-list',
  templateUrl: './author-list.component.html',
  styleUrls: ['./author-list.component.scss']
})
export class AuthorListComponent implements OnInit {

  authors : Observable<Author[]>;

  constructor(private authorService : AuthorsService) { }

  ngOnInit(): void {
    this.authors = this.authorService.getAuthors();
    //this.authors.subscribe;
  }

}
