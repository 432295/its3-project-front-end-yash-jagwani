import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Customer } from 'src/app/model/Customer';
import { CustomersService } from 'src/app/service/customers.service';

@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.scss']
})
export class CustomerFormComponent {

  customer: Customer;

  constructor(
    private route: ActivatedRoute, 
      private router: Router, 
        private customerService: CustomersService) {
    this.customer = new Customer();
   }

   onSubmit() {
     this.customerService.save(this.customer).subscribe(result => this.gotoCustomerList());
   }

   gotoCustomerList() {
     this.router.navigate(['/customers']);
   }

}