import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
// import { BOOKS } from './mock-books';
import { Book } from '../model/Book';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  libraryURL = environment.libraryApiURL;

  constructor(private http: HttpClient) { }

  public getBooks(): Observable<Book[]>{
    return this.http.get<Book[]>(this.libraryURL+'/books');
  }

  public save(book: Book) {
    return this.http.post<Book>(this.libraryURL+'/books', book);
  }

  public getBook(id): Observable<any> {
    return this.http.get(`${this.libraryURL+'/books'}/${id}`);
  }

  public update(id, data): Observable<any> {
    return this.http.put(`${this.libraryURL+'/books'}/${id}`, data);
  }

  public delete(id): Observable<any> {
    return this.http.delete(`${this.libraryURL+'/books'}/${id}`);
  }

  public deleteAll(): Observable<any> {
    return this.http.delete(this.libraryURL+'/books');
  }

  public findByAuthor(author): Observable<any> {
    return this.http.get(`${this.libraryURL+'/books'}?author=${author}`);
  }
}
