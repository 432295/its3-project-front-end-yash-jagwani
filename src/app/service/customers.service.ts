import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
// import { CUSTOMERS } from './mock-customers';
import { Customer } from '../model/Customer';
// import { Book } from '../model/Book';
// import { Author } from '../model/Author';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  libraryURL = environment.libraryApiURL;

  constructor(private http: HttpClient) { }

  public getCustomers(): Observable<Customer[]>{
    return this.http.get<Customer[]>(this.libraryURL+'/customers');
  }

  public save(customer: Customer) {
    return this.http.post<Customer>(this.libraryURL+'/customers', customer);
  }

  // getBooks(): Observable<Book[]>{
  //   return this.http.get<Book[]>(this.libraryURL+'/books');
  // }

  // getAuthors(): Observable<Author[]>{
  //   return this.http.get<Author[]>(this.libraryURL+'/authors')
  // }
}
