import { Customer } from '../model/Customer'

const CUSTOMERS: Customer[] =
[
    {
        "id": 1,
        "customerNr": "C1:",
        "name": "Yash Jagwani-",
        "books": "Mathematics and The Physical World"
    },
    {
        "id": 2,
        "customerNr": "C2:",
        "name": "Joshua George-",
        "books": "12 Rules for Life"
    }
]

export {CUSTOMERS}