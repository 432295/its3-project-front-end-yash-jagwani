import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const AUTH_API = 'http://localhost:8085/api/auth/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(credentials): Observable<any> {
    return this.http.post(AUTH_API + 'signin', {
      username: credentials.username,
      password: credentials.password
    }, httpOptions);
  }

  register(user): Observable<any> {
    return this.http.post(AUTH_API + 'signup', {
      username: user.username,
      email: user.email,
      password: user.password
    }, httpOptions);
  }
}

// import { Injectable } from '@angular/core';
// import { HttpClient } from '@angular/common/http';
// import { BehaviorSubject, Observable } from 'rxjs';
// import { map } from 'rxjs/operators';

// import { User } from 'src/app/model/User';

// @Injectable({
//   providedIn: 'root'
// })
// export class AuthenticationService {
//   private currentUserSubject: BehaviorSubject<User>;
//   public currentUser: Observable<User>;

//   constructor(private http: HttpClient) {
//       this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
//       this.currentUser = this.currentUserSubject.asObservable();
//   }

//   public get currentUserValue(): User {
//       return this.currentUserSubject.value;
//   }

//   login(username: string, password: string) {
//       return this.http.post<any>(`/users/authenticate`, { username, password })
//           .pipe(map(user => {
//               // login successful if there's a jwt token in the response
//               if (user && user.token) {
//                   // store user details and jwt token in local storage to keep user logged in between page refreshes
//                   localStorage.setItem('currentUser', JSON.stringify(user));
//                   this.currentUserSubject.next(user);
//               }

//               return user;
//           }));
//   }

//   logout() {
//       // remove user from local storage to log user out
//       localStorage.removeItem('currentUser');
//       this.currentUserSubject.next(null);
//   }
// }
