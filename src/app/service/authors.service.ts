import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
// import { AUTHORS } from './mock-authors';
import { Author } from '../model/Author';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {

  libraryURL = environment.libraryApiURL;

  constructor(private http: HttpClient) { }

  getAuthors(): Observable<Author[]>{
    return this.http.get<Author[]>(this.libraryURL+'/authors')
  }

}
